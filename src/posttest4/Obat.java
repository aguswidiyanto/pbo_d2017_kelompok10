public class Obat {
    private String merk;
    private String harga;
    
    public Obat(String merk, String harga){
        this.merk = merk;
        this.harga = harga;   
    }
    public String getMerk() {
        return merk;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }
}