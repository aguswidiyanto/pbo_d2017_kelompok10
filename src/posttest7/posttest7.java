import static java.lang.System.out;

public class posttest7 {

    private static class alat_musik{
        public void suaranya(){
            out.println("Ada Band!!");
        }
    }

    private static class gitar extends alat_musik{
        @Override
        public void suaranya(){
            out.println("Jreeenggg");
        }
    }

    private static class ariel extends alat_musik{
        @Override
        public void suaranya(){
            out.println("Kalian luar biasaa~~");
        }
    }

    private static class drum extends alat_musik{
        @Override
        public void suaranya(){
            out.println("Ba Dum Tss");
        }
        
        public void suara(){
            out.println("Ini Suara Drum");
        }
    }

    private static void mainkan(alat_musik Alat_musik){
        if (Alat_musik instanceof drum){
            drum musik = (drum) Alat_musik;
            musik.suara();
        }
        else{
            out.println("Ini bukan drum!!1!1!");
        }
    }

    public static void main(String []args){
        alat_musik musixx;
        
            musixx = new alat_musik();
            musixx.suaranya();
            
            musixx = new gitar();
            musixx.suaranya();
            mainkan(musixx);
            
            musixx = new drum();
            musixx.suaranya();
            mainkan(musixx);
            
            musixx = new ariel();
            musixx.suaranya();
    }
}