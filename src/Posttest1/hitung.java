package Posttest1;

import java.util.Scanner;

public class hitung {
    Scanner input = new Scanner(System.in);         
    public void persegi(){
        double s , luas,keliling;
        System.out.println("\n–>Persegi <–");
        System.out.print("Masukkan Sisi : ");
        s = input.nextDouble();
        System.out.println("=>");
        luas = s*s;
        keliling = 4*s;
        System.out.print("Luas  = " + (double)luas + " \nKeliling  = "+ (double)keliling);
        System.out.println("");
    }
    public void lingkaran(){
        double r ,phi, luas,keliling;
        phi=3.14;
        System.out.println("\n–>Lingkaran<–");
        System.out.print("Masukkan jari-jari : ");
        r = input.nextDouble();
        System.out.println("=>");
        luas = phi * r * r;
        keliling = 2*phi*r;
        System.out.print("Luas  = " + (double)luas + " \nKeliling  = "+ (double)keliling);
        System.out.println("");
    }
    public void kubus(){
        double s , luasp,volume;
        System.out.println("\n–>Kubus<–");
        System.out.print("Masukkan sisi : ");
        s = input.nextDouble();
        System.out.println("=>");
        luasp = 6*s*s;
        volume = s*s*s;
        System.out.print("Luas Permukaan  = " + (double)luasp + " \nVolume  = "+ (double)volume);
        System.out.println("");
    }
}
